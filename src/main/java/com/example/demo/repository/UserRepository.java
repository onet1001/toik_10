package com.example.demo.repository;

import com.example.demo.model.User;

import java.util.HashMap;
import java.util.Map;

public class UserRepository {

    private final Map<Integer, User> usersDatabase;

    public UserRepository() {
        usersDatabase = new HashMap<>();

        usersDatabase.put(1, new User("cracker", "cracker1234", true, 0));
        usersDatabase.put(2, new User("marry", "marietta!#09", true, 0));
        usersDatabase.put(3, new User("silver", "$silver$", true, 0));
    }

    public String checkLogin(final String login, final String password) {
        for(Map.Entry<Integer, User> entry : usersDatabase.entrySet()) {
            User user = entry.getValue();
            if(user.getLogin().equals(login)){
                System.out.println(user);
                if(!user.isActive()){
                    return "suspended user";
                }
                if(user.getPassword().equals(password)){
                    return "correct";
                }else{
                    user.setIncorrectLoginCounter(user.getIncorrectLoginCounter()+1);
                    if(user.getIncorrectLoginCounter()==3){
                        user.setActive(false);
                    }
                    return "no such user";
                }
            }
        }
        return "no such user";
    }
}
